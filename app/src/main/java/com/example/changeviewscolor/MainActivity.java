package com.example.changeviewscolor;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText one;
    EditText two;
    EditText three;
    EditText foure;

    ArrayList<Integer> colors = new ArrayList<>(10);

    {
        colors.add(Color.CYAN);
        colors.add(Color.BLACK);
        colors.add(Color.GREEN);
        colors.add(Color.YELLOW);
        colors.add(Color.BLUE);
        colors.add(Color.DKGRAY);
        colors.add(Color.GRAY);
        colors.add(Color.MAGENTA);
        colors.add(Color.RED);
        colors.add(Color.WHITE);
    }

    ArrayList<Integer> setingColor = new ArrayList<>();

    {
        setingColor.add(colors.get(0));
        setingColor.add(colors.get(1));
        setingColor.add(colors.get(2));
        setingColor.add(colors.get(3));

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        one = (EditText) findViewById(R.id.editText);
        two = (EditText) findViewById(R.id.editText2);
        three = (EditText) findViewById(R.id.editText3);
        foure = (EditText) findViewById(R.id.editText4);


        one.setBackgroundColor(setingColor.get(0));
        two.setBackgroundColor(setingColor.get(1));
        three.setBackgroundColor(setingColor.get(2));
        foure.setBackgroundColor(setingColor.get(3));

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int a = 0; // Начальное значение диапазона - "от"
                int b = 10; // Конечное значение диапазона - "до"
                int random_number1;
                int random_number2;
                int random_number3;

                random_number1 = a + (int) (Math.random() * b); // Генерация 1-го числа
                random_number2 = a + (int) (Math.random() * b); // Генерация 2-го числа
                random_number3 = a + (int) (Math.random() * b); // Генерация 3-го числа

                if (!setingColor.contains(colors.get(random_number1))) {
                    setingColor.set(1, colors.get(random_number1));
                }

                if (!setingColor.contains(colors.get(random_number2))) {
                    setingColor.set(2, colors.get(random_number2));
                }
                if (!setingColor.contains(colors.get(random_number3))) {
                    setingColor.set(3, colors.get(random_number3));
                }

                two.setBackgroundColor(setingColor.get(1));
                three.setBackgroundColor(setingColor.get(2));
                foure.setBackgroundColor(setingColor.get(3));

            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = 0; // Начальное значение диапазона - "от"
                int b = 10; // Конечное значение диапазона - "до"
                int random_number1;
                int random_number2;
                int random_number3;



                    random_number1 = a + (int) (Math.random() * b); // Генерация 1-го числа

                    random_number2 = a + (int) (Math.random() * b); // Генерация 2-го числа

                    random_number3 = a + (int) (Math.random() * b); // Генерация 3-го числа


                if (!setingColor.contains(colors.get(random_number1))) {
                    setingColor.set(0, colors.get(random_number1));
                }

                if (!setingColor.contains(colors.get(random_number2))) {
                    setingColor.set(2, colors.get(random_number2));
                }
                if (!setingColor.contains(colors.get(random_number3))) {
                    setingColor.set(3, colors.get(random_number3));
                }

                one.setBackgroundColor(setingColor.get(0));
                three.setBackgroundColor(setingColor.get(2));
                foure.setBackgroundColor(setingColor.get(3));

            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = 0; // Начальное значение диапазона - "от"
                int b = 10; // Конечное значение диапазона - "до"
                int random_number1;
                int random_number2;
                int random_number3;


                random_number1 = a + (int) (Math.random() * b); // Генерация 1-го числа

                random_number2 = a + (int) (Math.random() * b); // Генерация 2-го числа

                random_number3 = a + (int) (Math.random() * b); // Генерация 3-го числа

                if (!setingColor.contains(colors.get(random_number1))) {
                    setingColor.set(0, colors.get(random_number1));
                }

                if (!setingColor.contains(colors.get(random_number2))) {
                    setingColor.set(1, colors.get(random_number2));
                }
                if (!setingColor.contains(colors.get(random_number3))) {
                    setingColor.set(3, colors.get(random_number3));
                }

                one.setBackgroundColor(setingColor.get(0));
                two.setBackgroundColor(setingColor.get(1));
                foure.setBackgroundColor(setingColor.get(3));


            }
        });
        foure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = 0; // Начальное значение диапазона - "от"
                int b = 10; // Конечное значение диапазона - "до"
                int random_number1;
                int random_number2;
                int random_number3;


                random_number1 = a + (int) (Math.random() * b); // Генерация 1-го числа

                random_number2 = a + (int) (Math.random() * b); // Генерация 2-го числа

                random_number3 = a + (int) (Math.random() * b); // Генерация 3-го числа

                if (!setingColor.contains(colors.get(random_number1))) {
                    setingColor.set(0, colors.get(random_number1));
                }

                if (!setingColor.contains(colors.get(random_number2))) {
                    setingColor.set(1, colors.get(random_number2));
                }
                if (!setingColor.contains(colors.get(random_number3))) {
                    setingColor.set(2, colors.get(random_number3));
                }

                one.setBackgroundColor(setingColor.get(0));
                three.setBackgroundColor(setingColor.get(2));
                two.setBackgroundColor(setingColor.get(1));


            }
        });
    }
}
